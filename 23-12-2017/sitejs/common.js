  $('.sendldr').hide();

  //success toast fn starts here
  function showsuccesstoast() {
    var x = document.getElementById("snackbarsuccs");
    x.className = "show";
    setTimeout(function() {
      x.className = x.className.replace("show", "");
    }, 4000);
  }

  //failure toast fn starts here
  function showerrtoast() {
    var x = document.getElementById("snackbarerror");
    x.className = "show";
    setTimeout(function() {
      x.className = x.className.replace("show", "");
    }, 4000);
  }

  //error toast for ip errors
  function showiperrtoast() {
    var x = document.getElementById("snackbarerror");
    x.className = "show";
    setTimeout(function() {
      x.className = x.className.replace("show", "");
    }, 3500);
  }
  var wishcount = Number(localStorage.wish_count ? localStorage.wish_count : 0);
  $(function() {
    $("body").append('<div id="snackbarsuccs"></div><div id="snackbarerror"></div>');
    $(".manufacturingprice,.manufacturingpricedit").keypress(function(e) {
      if ($(this).val().length > 5 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
      }
    });
    $(".top-mini-cart .number-items").text(wishcount);
    if (localStorage.wutkn) {
      $(".wishdrop").siblings().remove();
      wishdrop(1);
      wishdrop(2);
    } else {
      $(".cart-dd").hide();
    }
  });

  //logout fn starts here
  function logout() {
    $.ajax({
      url: logout_api,
      type: 'post',
      headers: {
        "content-type": 'application/json'
      },
      beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
      },
      success: function(data) {
        sessionStorage.clear();
        localStorage.clear();

      },
      error: function(data) {
        sessionStorage.clear();
        localStorage.clear();
        window.location.replace("index.html");
      }
    }).done(function(dataJson) {
      window.location.replace("index.html");
    });
  } //logout fn starts here

  if (localStorage.wutkn == undefined || localStorage.wutkn == '') {
    $('#welcomeid').hide();
    $('#registerid').show();
    $('#poweroff').hide();
  } else {
    $('#welcomeid').show();
    $('#registerid').hide();
    $('#poweroff').show();
  }



  //solitaries function starts here
  function solitaries(type) {
    // if (localStorage.wutkn == undefined || localStorage.wutkn == '') {
    //     if (type == 1) {
    //         $("#snackbarerror").text('Please login to view this page');
    //         showerrtoast();
    //         setTimeout(function() {
    //             window.location.href = "index.html"
    //         }, 3000);
    //     } else if (type == 2) {
    //         $("#snackbarerror").text('Please login to view this page');
    //         showerrtoast();
    //     }
    //
    // } else {
    localStorage.pageid_red = type;
    window.location.href = "solitaries.html"
    // }
  }

  //jewellery function starts here
  function jewelleries(type) {
    // if (localStorage.wutkn == undefined || localStorage.wutkn == '') {
    //     if (type == 1) {
    //         $("#snackbarerror").text('Please login to view this page');
    //         showerrtoast();
    //         setTimeout(function() {
    //             window.location.href = "index.html"
    //         }, 3000);
    //     } else if (type <= 7) {
    //         $("#snackbarerror").text('Please login to view this page');
    //         showerrtoast();
    //     }
    //
    // } else {
    if (type == 3) {
      sessionStorage.jewelcat = 4;
      localStorage.pageid_red = 6;
      window.location.href = "necklaces.html"
    } else if (type == 4) {
      sessionStorage.jewelcat = 1;
      localStorage.pageid_red = 7;
      window.location.href = "rings.html"
    } else if (type == 5) {
      sessionStorage.jewelcat = 3;
      localStorage.pageid_red = 8;
      window.location.href = "earrings.html"
    } else if (type == 6) {
      sessionStorage.jewelcat = 2;
      localStorage.pageid_red = 9;
      window.location.href = "bangles.html"
    } else if (type == 7) {
      sessionStorage.jewelcat = 5;
      localStorage.pageid_red = 10;
      window.location.href = "pendants.html"
    } else {
      window.location.href = "jewelleries.html"
    }
  }
  // }

  //jewellery function starts here
  function watches(type) {
    // if (localStorage.wutkn == undefined || localStorage.wutkn == '') {
    //     if (type == 1) {
    //         $("#snackbarerror").text('Please login to view this page');
    //         showerrtoast();
    //         setTimeout(function() {
    //             window.location.href = "index.html"
    //         }, 3000);
    //     } else if (type == 2) {
    //         $("#snackbarerror").text('Please login to view this page');
    //         showerrtoast();
    //     }
    //
    // } else {
    localStorage.pageid_red = 12;
    window.location.href = "watches.html"
  }
  // }


  //function for newletter validation starts here
  function newsletter() {
    if ($('.newsletteremailid').val() == '') {
      $('.newsletteremailid').addClass("iserr");
      $("#snackbarerror").text("Email Id is Required");
      showerrtoast();
      event.preventDefault();
      return;
    } else {
      var emailid = $('.newsletteremailid').val();
      if (emailid.indexOf("@") < 1 || emailid.lastIndexOf(".") < emailid.indexOf("@") + 2 || emailid.lastIndexOf(".") + 2 >= emailid.length) {
        $('.newsletteremailid').addClass('iserr');
        $("#snackbarerror").text("valid Email-Id is required");
        showerrtoast();
        event.stopPropagation();
        return;
      }
    }
    $('.sendldr').show();
    $(".newsltrsendbtn").attr("disabled", true);
    var postData = JSON.stringify({
      "email": $('.newsletteremailid').val()
    });
    $.ajax({
      url: newsletter_api,
      type: 'post',
      data: postData,
      headers: {
        "content-type": 'application/json',
      },
      success: function(data) {
        $("#snackbarsuccs").text("Subscription done successfully");
        showsuccesstoast();
        $('.sendldr').hide();
        $(".newsltrsendbtn").attr("disabled", false);
        $('.newsletteremailid').val('');

      },
      error: function(data) {
        for (var key in JSON.parse(data.responseText)) {
          $("#snackbarerror").text((JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0])[0]);
        }
        showerrtoast();
        $('.sendldr').hide();
        $(".newsltrsendbtn").attr("disabled", false);
      }
    });
  } //function for newletter validation ends here


  function getid(id) {
    sessionStorage.jewelcat = id;
    switch (id) {
      case 1:
        localStorage.pageid_red = 7;
        window.location.href = "rings.html";
        break;
      case 2:
        localStorage.pageid_red = 9;
        window.location.href = "bangles.html";
        break;
      case 3:
        localStorage.pageid_red = 8;
        window.location.href = "earrings.html";
        break;
      case 4:
        localStorage.pageid_red = 6;
        window.location.href = "necklaces.html";
        break;
      case 5:
        localStorage.pageid_red = 10;
        window.location.href = "pendants.html";
        break;
    }
  }

  function wishdrop(x) {
    $.ajax({
      url: listwish_api + x,
      type: 'get',
      headers: {
        "Authorization": "Token " + localStorage.wutkn
      },
      success: function(data) {
        console.log(data);
        if (data.length != 0) {
          $(".wishdrop").before(`
                    <div class="items position-relative mark-border">
                      <div class="items-inner">
                          <div class="cart-item-image">
                              <a href="#">
                                  <img src="${data[0].tagged_object.images[0].image}" alt="" class="cartprodimages">
                              </a>
                          </div>
                          <div class="cart-item-info">
                              <h4 class="font16 cartprodtitle"><a class="product-name font-montserrat"></a></h4>
                              <h5 class="font-montserrat-light mb3">${data[0].tagged_object.name}</h5>
                          </div>
                      </div>
                  </div>
                `);
        }
      },
      error: function(data) {

        for (var key in JSON.parse(data.responseText)) {
          $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
      }
    });
  }
