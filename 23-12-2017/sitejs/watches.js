$(function() {
  $(".ldmicon,.ldmldr").hide();
  // list(0);
});

//show catalogue function starts here
if (!localStorage.wutkn) {
  $('.loadcategory').show();
}else{
  $('.loadcategory').hide();
  list(0);
}

function showfunction() {
  localStorage.pageid_red = 12;
  $("#snackbarerror").text("LOGIN TO VIEW CATALOGUE");
  showerrtoast();
}


function list(type) {
  if (type == 0) {
    var Url = listWatches_api;
    $(".list_watches").empty();
  } else {
    var Url = sessionStorage.nexturl;
  }
  $.ajax({
    url: Url,
    type: 'get',
    headers: {
      "Authorization": "Token " + localStorage.wutkn
    },
    success: function(data) {
      if (data.next_url) {
        sessionStorage.nexturl = data.next_url;
        $(".ldmicon").show();
      } else {
        $(".ldmicon").hide();
      }
      $(".nulldata").remove();
      if (data.results.length == 0) {
        $(".list_watches").append(`
                    <center class="nulldata"><img src="nodata.png" class="img-responsive"></center>
                `);
      } else {
        for (var i = 0; i < data.results.length; i++) {
          $(".list_watches").append(`
                        <div class="item col-lg-2 col-md-4 col-sm-6  col-xs-12">
                            <div class="product-img position-relative">
                                <a class="font16 pointer" data-toggle="modal" data-target="#watchmodal" onclick="watchbigimage('${data.results[i].images[0].image}');">
                                    <img class="img-responsive" src="${data.results[i].images.length!=0?data.results[i].images[0].image:''}" alt="" style="height:170px;">
                                </a>
                                <div class="icon-wishlist position-absolute ${data.results[i].is_wish==true?'sparkle':''}">
                                    <a class="pointer" onclick="wish(this,${data.results[i].id})">
                                        <i class="fa fa-heart ${data.results[i].is_wish==true?'wishtrue':''}" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product-content text-item whitebg">
                                <h5>
                                    <a class="font-montserrat font15 pl15 pointer">${data.results[i].name}</a>
                                </h5>
                                <center>
                                    <div class="rating">
                                        <ul class="list-inline mb2 staring${data.results[i].id}">

                                        </ul>
                                    </div>
                                    <div>
                                        <button class="btn eqbtn" data-toggle="modal" data-target="#enquirymodal" onclick="enquiryfunc(${data.results[i].id})">ADD TO CART</button>
                                    </div>
                                </center>
                            </div>
                        </div>
                    `);
          starz(data.results[i].rating, data.results[i].id, 'staring');
        }
      }
      $(".ldmldr").hide();
    },
    error: function(data) {
      $(".nulldata").remove();
      $(".list_watches").append(`
                    <center class="nulldata"><img src="nodata.png"></center>
                `);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}

function proddetail(id) {
  sessionStorage.prodid = id;
  sessionStorage.type = 2;
  window.location.href = "product-detail.html";
}

function wish(el, id) {
  $.ajax({
    url: wishlist_api + id + "/" + "?type=2",
    type: 'post',
    headers: {
      "content-type": "application/json",
      "Authorization": "Token " + localStorage.wutkn
    },
    success: function(data) {
      if (data.in_wish_list == true) {
        $(el).children().addClass('wishtrue');
        $(el).parent().addClass('sparkle');
        $("#snackbarsuccs").text("Added to wishlist");
        showsuccesstoast();
        localStorage.wish_count = Number(localStorage.wish_count) + 1;
        $(".top-mini-cart .number-items").text(Number($(".top-mini-cart .number-items").text()) + 1);
      } else {
        $(el).children().removeClass('wishtrue');
        $(el).parent().removeClass('sparkle');
        $("#snackbarsuccs").text("Removed from wishlist");
        showsuccesstoast();
        localStorage.wish_count = Number(localStorage.wish_count) - 1;
        $(".top-mini-cart .number-items").text(Number($(".top-mini-cart .number-items").text()) - 1);
      }
      $(".wishdrop").siblings().remove();
      wishdrop(1);
      wishdrop(2);
    },
    error: function(data) {

      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}

//function for star append
function starz(star, id, cl) {
  $("." + cl + id).empty();
  if (star) {
    var whole = Math.floor(star);
    var high = 5 - Math.ceil(star);
    var dec = (star * 10) % 10;
    for (var i = 0; i < whole; i++) {
      $("." + cl + id).append(`<li><a class="fa fa-star"></a></li>`);
    }
    if (dec > 4) {
      $("." + cl + id).append(`<li><a class="fa fa-star-half-o"></a></li>`);
    } else if (dec < 5 && dec != 0) {
      $("." + cl + id).append(`<li><a class="fa fa-star-o"></a></li>`);
    }
    for (var i = 0; i < high; i++) {
      $("." + cl + id).append(`<li><a class="fa fa-star-o"></a></li>`);
    }
  } else {
    for (var i = 0; i < 5; i++) {
      $("." + cl + id).append(`<li><a class="fa fa-star-o"></a></li>`);
    }
  }
}

$("#quantity,#description").keyup(function(event) {
  $(this).val() ? $(this).removeClass("iserr") : $(this).addClass("iserr");
  event.keyCode == 13 ? $(".sendbtn").click() : '';
});

//send enquiry func starts here
function sendenquiry() {
  if ($("#quantity").val() == "") {
    $("#quantity").addClass("iserr");
    $("#snackbarerror").text("Quantity is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($("#description").val() == "") {
    $("#description").addClass("iserr");
    $("#snackbarerror").text("Description is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  var postData = JSON.stringify({
    "description": $('#description').val(),
    "quantity": $('#quantity').val()
  });
  $(".sendldr").show();
  $(".sendbtn").attr("disabled", true);

  $.ajax({
    url: makeenquiry_api + editid + '/?type=2',
    type: 'POST',
    data: postData,
    headers: {
      "Authorization": "Token " + localStorage.wutkn,
      "content-type": 'application/json'
    },
    success: function(data) {
      $(".sendldr").hide();
      $(".sendbtn").attr("disabled", false);
      $('.enquiryclose').click();
    },
    error: function(data) {
      $(".sendldr").hide();
      $(".sendbtn").attr("disabled", false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  })
} //func ends here

var editid;
//enquiry function starts here
function enquiryfunc(val) {
  editid = val;
}

function loadmore() {
  $(".ldmldr").show();
  list(1);
}


//function to load watch image
function watchbigimage(src){
   $('.imgappendclass').attr("src",src);
}
