$(function() {
  $(".ldmicon" + sessionStorage.jewelcat).hide();
  $(".ldmldr,.ldmicon1").hide();
});

//show catalogue function starts here
if (!localStorage.wutkn) {
  $('.loadcategory').show();
}else{
  $('.loadcategory').hide();
  list(0);
}

function showfunction() {
  if(sessionStorage.jewelcat == 4){
    localStorage.pageid_red = 6;
  }else if(sessionStorage.jewelcat == 1){
    localStorage.pageid_red = 7;
  }else if(sessionStorage.jewelcat == 3){
    localStorage.pageid_red = 8;
  }else if(sessionStorage.jewelcat == 2){
    localStorage.pageid_red = 9;
  }else if(sessionStorage.jewelcat == 5){
    localStorage.pageid_red = 10;
  }
  $("#snackbarerror").text("LOGIN TO VIEW CATALOGUE");
  showerrtoast();
}




function list(type, cat) {
  if (type == 0) {
    var Url = listjewels_api + sessionStorage.jewelcat + '/';
    $(".list" + cat).empty();
  } else {
    var Url = sessionStorage.nexturl;
  }
  $.ajax({
    url: Url,
    type: 'get',
    headers: {
      "Authorization": "Token " + localStorage.wutkn
    },
    success: function(data) {
      if (data.next_url) {
        sessionStorage.nexturl = data.next_url;
        $(".ldmicon" + sessionStorage.jewelcat).show();
      } else {
        $(".ldmicon" + sessionStorage.jewelcat).hide();
      }
      $(".nulldata").remove();
      if (data.results.length == 0) {
        $(".list" + sessionStorage.jewelcat).append(`
                    <center class="nulldata"><img src="nodata.png" class="img-responsive"></center>
                `);
      } else {
        for (var i = 0; i < data.results.length; i++) {
          $(".list" + sessionStorage.jewelcat).append(`
                        <div class="item col-lg-2 col-md-4 col-sm-6  col-xs-12">
                            <div class="product-img position-relative">
                                <a onclick="proddetail(${data.results[i].id})" class="font16 pointer">
                                    <img class="img-responsive" src="${data.results[i].images[0].image}" alt="" style="height:170px;">
                                </a>
                                <div class="icon-wishlist position-absolute ${data.results[i].is_wish==true?'sparkle':''}">
                                    <a class="pointer" onclick="wish(this,${data.results[i].id})">
                                        <i class="fa fa-heart ${data.results[i].is_wish==true?'wishtrue':''}" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product-content text-item whitebg">
                                <h5>
                                    <a onclick="proddetail(${data.results[i].id})" class="font-montserrat font15 pl15 pointer">${data.results[i].name}</a>
                                </h5>
                                <center>
                                    <div class="rating">
                                        <ul class="list-inline mb2 staring${data.results[i].id}">

                                        </ul>
                                    </div>
                                </center>
                                <div>
                                    <button class="btn eqbtn" data-toggle="modal" data-target="#enquirymodal" onclick="enquiryfunc(11)">ADD TO CART</button>
                                </div>
                            </div>
                        </div>
                    `);
          starz(data.results[i].rating, data.results[i].id, 'staring');

        }
      }
      $(".ldmldr").hide();
    },
    error: function(data) {
      $(".nulldata").remove();
      $(".list" + cat).append(`
                    <center class="nulldata"><img src="nodata.png" class="img-responsive"></center>
                `);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}

function proddetail(id) {
  sessionStorage.prodid = id;
  sessionStorage.type = 1;
  window.location.href = "product-detail.html";
}

function wish(el, id) {
  $.ajax({
    url: wishlist_api + id + "/" + "?type=1",
    type: 'post',
    headers: {
      "content-type": "application/json",
      "Authorization": "Token " + localStorage.wutkn
    },
    success: function(data) {
      if (data.in_wish_list == true) {
        $(el).children().addClass('wishtrue');
        $(el).parent().addClass('sparkle');
        $("#snackbarsuccs").text("Added to wishlist");
        showsuccesstoast();
        localStorage.wish_count = Number(localStorage.wish_count) + 1;
        $(".top-mini-cart .number-items").text(Number($(".top-mini-cart .number-items").text()) + 1);
      } else {
        $(el).children().removeClass('wishtrue');
        $(el).parent().removeClass('sparkle');
        $("#snackbarsuccs").text("Removed from wishlist");
        showsuccesstoast();
        localStorage.wish_count = Number(localStorage.wish_count) - 1;
        $(".top-mini-cart .number-items").text(Number($(".top-mini-cart .number-items").text()) - 1);
      }
      $(".wishdrop").siblings().remove();
      wishdrop(1);
      wishdrop(2);
    },
    error: function(data) {

      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}

//function for star append
function starz(star, id, cl) {
  $("." + cl + id).empty();
  if (star) {
    var whole = Math.floor(star);
    var high = 5 - Math.ceil(star);
    var dec = (star * 10) % 10;
    for (var i = 0; i < whole; i++) {
      $("." + cl + id).append(`<li><a class="fa fa-star"></a></li>`);
    }
    if (dec > 4) {
      $("." + cl + id).append(`<li><a class="fa fa-star-half-o"></a></li>`);
    } else if (dec < 5 && dec != 0) {
      $("." + cl + id).append(`<li><a class="fa fa-star-o"></a></li>`);
    }
    for (var i = 0; i < high; i++) {
      $("." + cl + id).append(`<li><a class="fa fa-star-o"></a></li>`);
    }
  } else {
    for (var i = 0; i < 5; i++) {
      $("." + cl + id).append(`<li><a class="fa fa-star-o"></a></li>`);
    }
  }
}


function loadmore() {
  $(".ldmldr").show();
  list(1);
}
