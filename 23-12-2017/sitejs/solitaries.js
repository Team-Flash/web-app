var rangeval, hold_id, rateval, currval, sortvalue = "",
  symbol = "Rs";

if (rangeval == "" || rangeval == undefined || rangeval.length == 0) {
  rangeval = $('#number_range .range-picker .label.select-label').html();
}

if ($('#number_range .range-picker .label.select-label').html() == 10) {
  $('.caratslider').css("width", "20px");
}
if ($('#number_range .range-picker .label.select-label').html() == 20) {
  $('.caratslider').css("width", "30px");
}
//key up for carat starts here
$('.caratslider ').css("width", 20 + "%");

// var carat_val = [".9","1.8",""]
$('.getvalue2').keyup(function() {
  var getvalue = parseFloat($('.getvalue2').val());
  if ((0 <= getvalue) && (getvalue <= 0.2)) {
    $('.caratslider ').css("width", parseInt(1) + "%");
  } else if ((0.2 < getvalue) && (getvalue <= 0.25)) {
    $('.caratslider ').css("width", parseInt(2 * 0.9) + "%");
  } else if ((0.25 < getvalue) && (getvalue <= 0.3)) {
    $('.caratslider ').css("width", parseInt(3 * 0.9) + "%");
  } else if ((0.3 < getvalue) && (getvalue <= 0.4)) {
    $('.caratslider ').css("width", parseInt(4 * 0.9) + "%");
  } else if ((0.4 < getvalue) && (getvalue <= 0.5)) {
    $('.caratslider ').css("width", parseInt(5 * 0.9) + "%");
  } else if ((0.5 < getvalue) && (getvalue <= 0.6)) {
    $('.caratslider ').css("width", parseInt(6 * 0.9) + "%");
  } else if ((0.6 < getvalue) && (getvalue <= 0.8)) {
    $('.caratslider ').css("width", parseInt(7 * 0.9) + "%");
  } else if ((0.8 < getvalue) && (getvalue <= 1.0)) {
    $('.caratslider ').css("width", parseInt(8 * 0.9) + "%");
  } else if ((1.0 < getvalue) && (getvalue <= 1.25)) {
    $('.caratslider ').css("width", parseInt(9 * 0.9) + "%");
  } else if ((1.25 < getvalue) && (getvalue <= 2)) {
    $('.caratslider ').css("width", parseInt(10 * 0.9) + "%");
  } else if ((2 < getvalue) && (getvalue <= 3)) {
    $('.caratslider ').css("width", parseInt(11 * 0.9) + "%");
  } else if ((3 < getvalue) && (getvalue <= 4)) {
    $('.caratslider ').css("width", parseInt(12 * 0.9) + "%");
  } else if ((4 < getvalue) && (getvalue <= 5)) {
    $('.caratslider ').css("width", parseInt(13 * 0.9) + "%");
  } else if ((5 < getvalue) && (getvalue <= 6)) {
    $('.caratslider ').css("width", parseInt(14 * 0.9) + "%");
  } else if ((6 < getvalue) && (getvalue <= 7)) {
    $('.caratslider ').css("width", parseInt(15 * 0.9) + "%");
  } else if ((7 < getvalue) && (getvalue <= 8)) {
    $('.caratslider ').css("width", parseInt(16 * 0.9) + "%");
  } else if ((8 < getvalue) && (getvalue <= 9)) {
    $('.caratslider ').css("width", parseInt(17 * 0.9) + "%");
  } else if ((9 < getvalue) && (getvalue <= 10)) {
    $('.caratslider ').css("width", parseInt(18 * 0.9) + "%");
  } else if ((10 < getvalue) && (getvalue <= 11)) {
    $('.caratslider ').css("width", parseInt(19 * 0.9) + "%");
  } else if ((11 < getvalue) && (getvalue <= 12)) {
    $('.caratslider ').css("width", parseInt(20 * 0.9) + "%");
  } else if ((12 < getvalue) && (getvalue <= 13)) {
    $('.caratslider ').css("width", parseInt(21 * 0.9) + "%");
  } else if ((13 < getvalue) && (getvalue <= 14)) {
    $('.caratslider ').css("width", parseInt(22 * 0.9) + "%");
  } else if ((14 < getvalue) && (getvalue <= 15)) {
    $('.caratslider ').css("width", parseInt(23 * 0.9) + "%");
  } else {
    $('.caratslider ').css("width", 20 + "%");
  }
})
var shape = [],
  symmetry = [],
  fluorescence = [],
  cut = [],
  clarity = [],
  color = [],
  polish = [],
  editid, pay_id, currencyval = [],
  fromrange, torange, colval = ["d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];

fromrange = $('#double_number_range .label.select-label').eq(0).text();
torange = $('#double_number_range .label.select-label').eq(1).text();

$.ajax({
  url: "http://api.fixer.io/latest?base=USD",
  type: 'GET',
  success: function(data) {
    console.log(data);
    currencyval = data;
    $("#to_id").val(parseInt(400000 * currencyval.rates.INR));
    $("#to_id1").val(parseInt(400000 * currencyval.rates.GBP));
    $("#to_id2").val(parseInt(400000 * currencyval.rates.EUR));
    $("#to_id3").val(400000);
    currval = currencyval.rates.INR;
    rangesliders();
    rangesliders1();
    rangesliders2();
    rangesliders3();

  },
  failure: function(data) {
    for (var key in JSON.parse(data.responseText)) {
      $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
    }
    showerrtoast();
  }
});


$('.getvalue1').keyup(function(e) {
  //getting key code of pressed key
  if (localStorage.wutkn) {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);
  }

});
$('.getvalue2').keyup(function() {
  if (localStorage.wutkn) {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);
  }

})

//load initial function
function showfunction() {
  if (localStorage.wutkn) {
    listsearchresults(0);
    $('#table_showid').show();
    $('.loadcategory').hide();
  } else {
    localStorage.pageid_red = 11;
    $("#snackbarerror").text("LOGIN TO VIEW THE SEARCH");
    showerrtoast();
  }
}


$(function() {
  $('#table_showid').hide();

  //drop down works on
  $('#double_number_range').show();
  $('#double_number_range1,#double_number_range2,#double_number_range3').hide();

  $('.classone').click(function() {
    symbol = "Rs";
    $('.symbol_text').text("Rs.");
    $('.classoneimg').addClass("active");
    $('.classtwoimg,.classthreeimg,.classfourimg').removeClass("active");
    currval = currencyval.rates.INR;
    $('#double_number_range').show();
    $('#double_number_range1,#double_number_range2,#double_number_range3').hide();
    if (localStorage.wutkn) {
      listsearchresults(0);
    }
  });
  $('.classtwo').click(function() {
    symbol = "£";
    $('.symbol_text').text("£");

    $('.classtwoimg').addClass("active");
    $('.classoneimg,.classthreeimg,.classfourimg').removeClass("active");
    currval = currencyval.rates.GBP;
    $('#double_number_range1').show();
    $('#double_number_range2,#double_number_range,#double_number_range3').hide();
    if (localStorage.wutkn) {
      listsearchresults(0);
    }

  })

  $('.classthree').click(function() {
    symbol = "€";
    $('.symbol_text').text("€");

    $('.classoneimg,.classtwoimg,.classfourimg').removeClass("active");
    $('.classthreeimg').addClass("active");
    currval = currencyval.rates.EUR;
    $('#double_number_range2').show();
    $('#double_number_range1,#double_number_range,#double_number_range3').hide();
    if (localStorage.wutkn) {
      listsearchresults(0);
    }

  })

  $('.classfour').click(function() {
    symbol = "$";
    $('.symbol_text').text("$");

    $('.classoneimg,.classtwoimg,.classthreeimg').removeClass("active");
    $('.classfourimg').addClass("active");
    currval = 1;
    $('#double_number_range3').show();
    $('#double_number_range1,#double_number_range,#double_number_range2').hide();
    if (localStorage.wutkn) {
      listsearchresults(0);
    }

  })

});

//add product starts here
function listsearchresults(type) {
  if (type == 0) {
    var url = solitaries_api;
  } else {
    var url = type;
  }
  var postData = JSON.stringify({

    "price": [fromrange, torange],
    "symmetry": symmetry,
    "fluorescence": fluorescence,
    "cut": cut,
    "clarity": clarity,
    "color": color,
    "shape": shape,
    "polish": polish,
    "carat": {
      "from": $('.getvalue1').val(),
      "to": $('.getvalue2').val()
    },
    "sort": sortvalue
  });


  $.ajax({
    url: url,
    type: 'POST',
    data: postData,
    headers: {
      "Authorization": "Token " + localStorage.wutkn,
      "content-type": 'application/json'
    },
    success: function(data) {
      $('.tablebodyclass').empty();
      if (data.results.length != 0) {
        $('.tableheadclass').show();
        $(".total_price").addClass(data.results[0].price_sort);
        for (var i = 0; i < data.results.length; i++) {
          $('.tablebodyclass').append(` <tr>
                                        <td>${data.results[i].stock_id}</td>
                                        <td>${data.results[i].shape}</td>
                                        <td>${data.results[i].weight}</td>
                                        <td>${data.results[i].color}</td>
                                        <td>${data.results[i].clarity}</td>
                                        <td>${data.results[i].cut}</td>
                                        <td>${data.results[i].polish}</td>
                                        <td>${data.results[i].symmetry}</td>
                                        <td>${symbol} ${(data.results[i].total_price * currval).toFixed(2)}</td>
                                        <td>${data.results[i].lab}</td>
                                        <td>${data.results[i].fluorescence}</td>
                                        <td>${data.results[i].total_depth}%</td>
                                        <td>${data.results[i].table}%</td>
                                        <td>${data.results[i].measurements}</td>
                                        <td><button onclick="placeorder(${data.results[i].id})" class="add-to-cart btn bg-black color-white btn--ys btn--md font-montserrat-light text-uppercase font14 buynow-btn">Buy Now</button></td>
                                        <td>
                                            <button onclick="holdfunc(${data.results[i].id});" class="email bg-black color-white btn--ys btn--md font18 buynow-btn primarycolorbtn holdbtn${data.results[i].id}" type="submit">Hold Now<i class="fa fa-spinner fa-spin fa-3x fa-fw btnldr holdloader${data.results[i].id}" style="display: none;"></i></button>
                                        </td>
                                        <td>
                                            <button onclick="enquiryfunc(${data.results[i].id});" class="email bg-black color-white btn--ys btn--md font18 buynow-btn primarycolorbtn" data-toggle="modal" data-target="#enquirymodal" type="submit">Enquire Now</button>
                                        </td>
                                    </tr>`);
        }

        if (data.next_url != null) {
          var next1 = (data.next_url).split('=');
          var val = data.count / data.page_size;
          if (val["colorField"] % 1 === 0) {
            //if number is integer
            var val = parseInt(val) + 1;
          } else {
            var val = val;
          }
          var obj = $('#pagination').twbsPagination({
            totalPages: val,
            visiblePages: 5,
            onPageClick: function(event, page) {
              console.info(page);
              listsearchresults(next1[0] + "=" + (page));
            }
          });
          console.info(obj.data());
        }
      } else {
        $('.tableheadclass').hide();
        $('.tablebodyclass').append(`<center><p>No Data Found</p></center>`);
      }

    },
    error: function(data) {
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();

    }
  });
} //add product ends here


$('input[name="checkbox-option1"]').change(function() {
  if (shape.indexOf($(this).val()) == -1) {
    shape.push($(this).val());
  } else {
    for (var i = shape.length; i--;) {
      if (shape[i] === $(this).val()) shape.splice(i, 1);
    }
  }
  if (localStorage.wutkn) {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);
  }


});
$('input[name="checkbox-option2"]').change(function() {
  if (cut.indexOf($(this).val()) == -1) {
    cut.push($(this).val());
  } else {
    for (var i = cut.length; i--;) {
      if (cut[i] === $(this).val()) cut.splice(i, 1);
    }
  }
  if (localStorage.wutkn) {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);
  }

});
$('input[name="checkbox-option3"]').change(function() {
  $('.excellentsection,.vgoodsection,.goodsection,.fairsection,.poorsection,.ifsection,.vvssection,.vssection,.flsection,.vvs2section,.vs2section,.si1section,.si2section,.li1section,.li2section,.li3section').hide();
  $('.dsection').show();
  $('.nameclass').text($(this).val());
  $('.dsection').find('img').attr("src", "images/solitaries/" + $(this).data("get") + ".png");
  if (color.indexOf($(this).val()) == -1) {
    color.push($(this).val());
  } else {
    for (var i = color.length; i--;) {
      if (color[i] === $(this).val()) color.splice(i, 1);
    }
  }
  if (localStorage.wutkn) {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);
  }


});
$('input[name="checkbox-option4"]').change(function() {
  if (clarity.indexOf($(this).val()) == -1) {
    clarity.push($(this).val());
  } else {
    for (var i = clarity.length; i--;) {
      if (clarity[i] === $(this).val()) clarity.splice(i, 1);
    }
  }
  if (localStorage.wutkn) {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);
  }

});
$('input[name="checkbox-option5"]').change(function() {
  if (fluorescence.indexOf($(this).val()) == -1) {
    fluorescence.push($(this).val());
  } else {
    for (var i = fluorescence.length; i--;) {
      if (fluorescence[i] === $(this).val()) fluorescence.splice(i, 1);
    }
  }
  if (localStorage.wutkn) {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);
  }

});
$('input[name="checkbox-option6"]').change(function() {
  if (polish.indexOf($(this).val()) == -1) {
    polish.push($(this).val());
  } else {
    for (var i = polish.length; i--;) {
      if (polish[i] === $(this).val()) polish.splice(i, 1);
    }
  }
  if (localStorage.wutkn) {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);
  }

});
$('input[name="checkbox-option7"]').change(function() {
  if (symmetry.indexOf($(this).val()) == -1) {
    symmetry.push($(this).val());
  } else {
    for (var i = symmetry.length; i--;) {
      if (symmetry[i] === $(this).val()) symmetry.splice(i, 1);
    }
  }
  if (localStorage.wutkn) {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);
  }

});

//remove filter
function searchall() {
  $('.hide-checkbox:checked+label').css({
    "background-color": "#fff",
    "color": "black"
  });

  shape = [];
  symmetry = [];
  fluorescence = [];
  cut = [];
  clarity = [];
  color = [];
  polish = [];

  if (localStorage.wutkn) {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);
  }

}

//enquiry function starts here
function enquiryfunc(val) {
  editid = val;
}

//send enquiry func starts here
function sendenquiry() {

  if ($('#quantity').val() == "") {
    $("#snackbarerror").text("Please enter the quantity");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#description').val() == "") {
    $("#snackbarerror").text("Please enter the description");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  var postData = JSON.stringify({
    "description": $('#description').val(),
    "quantity": $('#quantity').val()
  });
  $(".sendldr").show();
  $(".sendbtn").attr("disabled", true);

  $.ajax({
    url: makeenquiry_api + editid + '/?type=3',
    type: 'POST',
    data: postData,
    headers: {
      "Authorization": "Token " + localStorage.wutkn,
      "content-type": 'application/json'
    },
    success: function(data) {
      $(".sendldr").hide();
      $(".sendbtn").attr("disabled", false);
      $('.enquiryclose').click();
    },
    failure: function(data) {
      $(".sendldr").hide();
      $(".sendbtn").attr("disabled", false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  })
} //func ends here



//place order fn starts here
function placeorder(id) {
  pay_id = id;
  //online payment fn starts here
  $.ajax({
    url: payment_api + id + '/',
    type: 'GET',
    headers: {
      "content-type": "application/json",
      "Authorization": 'Token ' + localStorage.wutkn
    },
    success: function(data) {
      $(".paymentldr").hide();
      $(".paymentBtn").attr("disabled", false);
      var prefilldata = JSON.parse(localStorage.userdetails);
      localStorage.userid = prefilldata.id;
      localStorage.userpno_payment = prefilldata.username;
      localStorage.useremail_payment = prefilldata.email;
      localStorage.userfname_payment = prefilldata.first_name;


      localStorage.finalamount = data.amount;
      localStorage.userorderid_payment = data.order_id;

      //razor pay fn starts here
      var options = {
        "key": "rzp_test_twhGB9MZHnXYT0",
        "amount": localStorage.finalamount,
        "name": "Prismarc",
        "description": "Buy now",
        "image": "http://prismarc.billioncart.in/images/prismarc/prismarc-logo.svg",
        "order_id": localStorage.userorderid_payment,
        "currency": "USD",
        "handler": function(response) {
          // console.log(response);
          console.log(response.razorpay_payment_id);
          console.log(response.razorpay_order_id);
          console.log(response.razorpay_signature);

          var postData = JSON.stringify({
            "razorpay_payment_id": response.razorpay_payment_id,
            "razorpay_order_id": response.razorpay_order_id,
            "razorpay_signature": response.razorpay_signature,
            "solitaries_id": pay_id
          });

          $.ajax({
            url: paymentsuccess_api,
            type: 'POST',
            data: postData,
            headers: {
              "content-type": 'application/json',
              "Authorization": "Token " + localStorage.wutkn
            },
            success: function(data) {
              $("#snackbarsuccs").text("Payment Success");
              showsuccesstoast();
            },
            error: function(data) {
              console.log("Error Occured in Online Buynow after razorpay");
              var errtext = "";
              for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
              }
              $(".paymentldr").hide();
              $(".paymentBtn").attr("disabled", false);
              $("#snackbarerror").text('Payment failure');
              showerrtoast();

            }
          }).done(function(dataJson) {});
        },
        "prefill": {
          "uid": localStorage.userid,
          "contact": localStorage.userpno_payment,
          "name": localStorage.userfname_payment,
          "email": localStorage.useremail_payment
        },
        "notes": {
          "address": "Dolluz Payment"
        },
        "theme": {
          "color": "#0058ae"
        }
      };

      rzp1 = new Razorpay(options);

      rzp1.open();
    },
    error: function(data) {
      $(".paymentldr").hide();
      $(".paymentBtn").attr("disabled", false);
      console.log("error occured in checkout")
      var errtext = "";
      for (var key in JSON.parse(data.responseText)) {
        errtext = JSON.parse(data.responseText)[key][0];
      }
      $("#snackbarerror").text(errtext);
      showerrtoast();
    }
  });
} //place order fn starts here


//hold function starts HERE
function holdfunc(h_id) {
  hold_id = h_id;
  var postData = JSON.stringify({
    "solitaires": h_id
  });
  $(".holdloader" + h_id).show();
  $(".holdbtn").attr("disabled", true);

  $.ajax({
    url: holdsolitaries_api,
    type: 'POST',
    data: postData,
    headers: {
      "Authorization": "Token " + localStorage.wutkn,
      "content-type": 'application/json'
    },
    success: function(data) {
      $(".holdloader" + h_id).hide();
      $(".holdbtn").attr("disabled", false);
      $("#snackbarsuccs").text("Your Request Has Been Sent Successfully");
      showsuccesstoast();
      $('#urlloader').empty();
      $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
      listsearchresults(0);
    },
    failure: function(data) {
      $(".holdloader" + h_id).hide();
      $(".holdbtn").attr("disabled", false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  })
}



//range slider srcipt
function rangesliders() {
  var $range = $("#double_number_range .js-range-slider"),
    $from = $("#double_number_range .js-from"),
    $to = $("#double_number_range .js-to"),
    range,
    min = 0,
    max = $("#double_number_range .js-to").val(),
    from,
    to;

  var updateValues = function() {
    $from.prop("value", from);
    $to.prop("value", to);
  };

  $range.ionRangeSlider({
    type: "double",
    min: min,
    max: max,
    prettify_enabled: false,
    grid: true,
    grid_num: 10,
    onChange: function(data) {
      from = data.from;
      to = data.to;

      updateValues();
    }
  });

  range = $range.data("ionRangeSlider");

  var updateRange = function() {
    range.update({
      from: from,
      to: to
    });
  };

  $from.on("change", function() {
    from = +$(this).prop("value");
    if (from < min) {
      from = min;
    }
    if (from > to) {
      from = to;
    }

    updateValues();
    updateRange();
  });

  $to.on("change", function() {
    to = +$(this).prop("value");
    if (to > max) {
      to = max;
    }
    if (to < from) {
      to = from;
    }

    updateValues();
    updateRange();
  });
}

//slider 2 srcipt
function rangesliders1() {
  var $range = $("#double_number_range1 .js-range-slider"),
    $from = $("#double_number_range1 .js-from"),
    $to = $("#double_number_range1 .js-to"),
    range,
    min = 0,
    max = $("#double_number_range1 .js-to").val(),
    from,
    to;

  var updateValues = function() {
    $from.prop("value", from);
    $to.prop("value", to);
  };

  $range.ionRangeSlider({
    type: "double",
    min: min,
    max: max,
    prettify_enabled: false,
    grid: true,
    grid_num: 10,
    onChange: function(data) {
      from = data.from;
      to = data.to;

      updateValues();
    }
  });

  range = $range.data("ionRangeSlider");

  var updateRange = function() {
    range.update({
      from: from,
      to: to
    });
  };

  $from.on("change", function() {
    from = +$(this).prop("value");
    if (from < min) {
      from = min;
    }
    if (from > to) {
      from = to;
    }

    updateValues();
    updateRange();
  });

  $to.on("change", function() {
    to = +$(this).prop("value");
    if (to > max) {
      to = max;
    }
    if (to < from) {
      to = from;
    }

    updateValues();
    updateRange();
  });
}

//slider 3 srcipt
function rangesliders2() {
  var $range = $("#double_number_range2 .js-range-slider"),
    $from = $("#double_number_range2 .js-from"),
    $to = $("#double_number_range2 .js-to"),
    range,
    min = 0,
    max = $("#double_number_range2 .js-to").val(),
    from,
    to;

  var updateValues = function() {
    $from.prop("value", from);
    $to.prop("value", to);
  };

  $range.ionRangeSlider({
    type: "double",
    min: min,
    max: max,
    prettify_enabled: false,
    grid: true,
    grid_num: 10,
    onChange: function(data) {
      from = data.from;
      to = data.to;

      updateValues();
    }
  });

  range = $range.data("ionRangeSlider");

  var updateRange = function() {
    range.update({
      from: from,
      to: to
    });
  };

  $from.on("change", function() {
    from = +$(this).prop("value");
    if (from < min) {
      from = min;
    }
    if (from > to) {
      from = to;
    }

    updateValues();
    updateRange();
  });

  $to.on("change", function() {
    to = +$(this).prop("value");
    if (to > max) {
      to = max;
    }
    if (to < from) {
      to = from;
    }

    updateValues();
    updateRange();
  });
}
//slider 4 srcipt
function rangesliders3() {
  var $range = $("#double_number_range3 .js-range-slider"),
    $from = $("#double_number_range3 .js-from"),
    $to = $("#double_number_range3 .js-to"),
    range,
    min = 0,
    max = $("#double_number_range3 .js-to").val(),
    from,
    to;

  var updateValues = function() {
    $from.prop("value", from);
    $to.prop("value", to);
  };

  $range.ionRangeSlider({
    type: "double",
    min: min,
    max: max,
    prettify_enabled: false,
    grid: true,
    grid_num: 10,
    onChange: function(data) {
      from = data.from;
      to = data.to;

      updateValues();
    }
  });

  range = $range.data("ionRangeSlider");

  var updateRange = function() {
    range.update({
      from: from,
      to: to
    });
  };

  $from.on("change", function() {
    from = +$(this).prop("value");
    if (from < min) {
      from = min;
    }
    if (from > to) {
      from = to;
    }

    updateValues();
    updateRange();
  });

  $to.on("change", function() {
    to = +$(this).prop("value");
    if (to > max) {
      to = max;
    }
    if (to < from) {
      to = from;
    }

    updateValues();
    updateRange();
  });
}

//onchange done here
function testfunc(me) {
  fromrange = me.from / currval;
  torange = me.to / currval;
  if (localStorage.wutkn) {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);
    return me;
  }
}

$(".js-from,.js-to").change(function() {
  fromrange = parseInt($(".js-from").val()) / currval;
  torange = parseInt($(".js-to").val()) / currval;
  if (localStorage.wutkn) {
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);

  }
})


//FUNCTION TO SORT DataFUNCTION sortfunc(ME)
function sortfunc(me) {
  console.log(me);
  var sortby = $(me).data("value");
  if (sortby == 1) {
    $(me).removeClass("fa-sort-amount-desc");
    $(me).addClass("fa-sort-amount-asc");
    $(me).data("value", "0");
    if ($(me).parent().text().trim() == "Total Price") {
      sortvalue = $(".total_price").attr("class").slice(12);
    } else {
      sortvalue = $(me).parent().text().trim().replace(" ", "_").toLowerCase();
    }
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);
  } else {
    $(me).removeClass("fa-sort-amount-asc");
    $(me).addClass("fa-sort-amount-desc");
    $(me).data("value", "1");
    if ($(me).parent().text().trim() == "Total Price") {
      sortvalue = "-" + $(".total_price").attr("class").slice(12);
    } else {
      sortvalue = "-" + $(me).parent().text().trim().replace(" ", "_").toLowerCase();
    }
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination" id="pagination"></ul>`);
    listsearchresults(0);
  }

  console.log(sortby);

}
