$(function() {
    $(".maintable").hide();
    $(".main_row").append(`
        <center class="nulldata"><img src="nodata.png" class="img-responsive"></center>
    `);
    $(".nulldata").hide();
    wishlist(1);
    wishlist(2);
});

function wishlist(type) {
    $.ajax({
        url: listwish_api + type,
        type: 'get',
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            if (data.length == 0) {
                $(".nulldata").show();
            } else {
                $(".maintable").show();
                $(".nulldata").remove();
                for (var i = 0; i < data.length; i++) {
                    $(".wish_list").append(`
                <tr class="tr${data[i].tagged_object.id}n${type}">
                    <td class="slno"></td>
                    <td><img src="${data[i].tagged_object.images[0].image}" class="img-responsive w50"></td>
                    <td>${data[i].tagged_object.name}</td>
                    <td>${type==1?'Jewelleries':'Watches'}</td>
                    <td>${type==1?data[i].tagged_object.category.name:'-'}</td>
                    <td><a class="add-to-cart btn bg-black color-white btn--ys btn--md font-montserrat-light text-uppercase font14 wishlist-enquiry" onclick="proddetail(${data[i].tagged_object.id},${type})">Enquire Now</a></td>
                    <td>
                        <div class="product-remove"><a class="flaticon-cancel font15 pointer" data-target="#confirmationmodal" data-toggle="modal" onclick="getids(${data[i].tagged_object.id},${type})"></a></div>
                    </td>
                </tr>
            `);
                }
                slno();
            }
        },
        error: function(data) {
            $(".nulldata").remove();

            $(".maintable").hide();

            $(".main_row").append(`
                    <center class="nulldata"><img src="nodata.png" class="img-responsive"></center>
                `);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function getids(id, typ) {
    sessionStorage.getid = id;
    sessionStorage.type = typ;
}

function proddetail(id, typ) {
    sessionStorage.type = typ;
    sessionStorage.prodid = id;
    typ == 1 ? window.location.href = "product-detail.html" : $("#enquirymodal").modal('show');
}

function wish() {
    $.ajax({
        url: wishlist_api + sessionStorage.getid + "/" + "?type=" + sessionStorage.type,
        type: 'post',
        headers: {
            "content-type": "application/json",
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".delldr").hide();
            $(".delcls").click();
            localStorage.wish_count = Number(localStorage.wish_count) - 1;
            $(".top-mini-cart .number-items").text(Number($(".top-mini-cart .number-items").text()) - 1);
            $(".wish_list").empty();
            $(".maintable").hide();
            $(".main_row").append(`
                <center class="nulldata"><img src="nodata.png" class="img-responsive"></center>
            `);
            wishlist(1);
            wishlist(2);
            $("#snackbarerror").text("Removed from wishlist");
            showerrtoast();
            $(".wishdrop").siblings().remove();
            wishdrop(1);
            wishdrop(2);
        },
        error: function(data) {
            $(".delldr").hide();
            $(".delcls").click();
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function slno() {
    for (var i = 0; i < $(".slno").length; i++) {
        $(".slno").eq(i).text(i + 1);
    }
}


//send enquiry func starts here
function sendenquiry() {
    if ($("#quantity").val() == "") {
        $("#quantity").addClass("iserr");
        $("#snackbarerror").text("Quantity is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#description").val() == "") {
        $("#description").addClass("iserr");
        $("#snackbarerror").text("Description is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    var postData = JSON.stringify({
        "description": $('#description').val(),
        "quantity": $('#quantity').val()
    });
    $(".sendldr").show();
    $(".sendbtn").attr("disabled", true);

    $.ajax({
        url: makeenquiry_api + editid + '/?type=2',
        type: 'POST',
        data: postData,
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": 'application/json'
        },
        success: function(data) {
            $(".sendldr").hide();
            $(".sendbtn").attr("disabled", false);
            $('.enquiryclose').click();
        },
        error: function(data) {
            $(".sendldr").hide();
            $(".sendbtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    })
} //func ends here

var editid;
//enquiry function starts here
function enquiryfunc(val) {
    editid = val;
}