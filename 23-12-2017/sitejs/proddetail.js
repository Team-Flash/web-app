$(function() {
    retrieveproducts();
});

function retrieveproducts() {
    $.ajax({
        url: retriveproduct_api + sessionStorage.prodid + '/',
        type: 'get',
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".woocommerce-breadcrumb").find('a').eq(2).text(data.category.name);
            $(".woocommerce-breadcrumb").find('span').text(data.name);
            $(".prodname").text(data.name);
            $("#zoom").attr("src", data.images[0].image);
            $("#zoom").elevateZoom();
            for (var i = 0; i < data.images.length; i++) {
                $(".prodimg" + i).attr("src", data.images[i].image);
                $(".prodimg" + i).attr("data-image", data.images[i].image);
            }
            for (var i = data.images.length; i < 3; i++) {
                $(".prodimg" + i).hide();
            }
            $(".list_related").empty();
            data.is_wish == true ? $(".wishbtn").text("IN WISHLIST") : $(".wishbtn").text("ADD TO WISHLIST");
            $(".nulldata").remove();
            if (data.related_product.length == 0) {
                $(".list_related").append(`
                    <center class="nulldata"><img src="nodata.png" class="img-responsive"></center>
                `);
            } else {
                $(".list_related").append(`<div class="item col-md-1"></div>`);
                for (var i = 0; i < data.related_product.length; i++) {
                    $(".list_related").append(`
                        <div class="item col-lg-2 col-md-4 col-sm-6  col-xs-12">
                            <div class="product-img position-relative">
                                <a onclick="proddetail(${data.related_product[i].id})" class="font16 pointer">
                                    <img class="img-responsive" src="${data.related_product[i].images.length!=0?data.related_product[i].images[0].image:""}" alt="" style="height:170px;">
                                </a>
                                <div class="icon-wishlist position-absolute ${data.related_product[i].is_wish==true?'sparkle':''}">
                                    <a class="pointer" onclick="wish(this,${data.related_product[i].id})">
                                        <i class="fa fa-heart ${data.related_product[i].is_wish==true?'wishtrue':''}" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product-content text-item whitebg pb20">
                                <h5>
                                    <a onclick="proddetail(${data.related_product[i].id})" class="font-montserrat font15 pl15 pointer">${data.related_product[i].name}</a>
                                </h5>
                                <center>
                                    <div class="rating">
                                        <ul class="list-inline mb2 staring${data.related_product[i].id}">

                                        </ul>
                                    </div>
                                </center>
                            </div>
                        </div>
                    `);
                    starz(data.related_product[i].rating, data.related_product[i].id, 'staring');
                }
                $(".list_related").append(`<div class="item col-md-1"></div>`);
            }
            if (data.review.length == 0) {

            } else {
                $(".listreviews").empty();
                for (let i = 0; i < data.review.length; i++) {
                    $(".listreviews").append(`
                        <li>
                            <div class="user-comments">
                                <h4 class="heading-regular font15 font-montserrat text-normal" style="width: 260px;"> Hannah Jordan <span style="float: right;"> ${data.review[i].rating} <a class="fa fa-star"></a></span></h4>
                                <h6 class="font-montserrat-light font13">${data.review[i].comment}</h6>
                            </div>
                        </li>
                    `);
                }
            }
        },
        error: function(data) {
            $(".nulldata").remove();
            $(".list_related").append(`
                    <center class="nulldata"><img src="nodata.png"></center>
                `);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

$(".eWeight").keyup(function(event) {
    $(this).val() ? $(this).removeClass("iserr") : $(this).addClass("iserr");
});

$(".likt li").click(function() {
    $(".likt li").removeClass("licurrent current");
    $(this).addClass("licurrent");
});

function enquire() {
    if ($(".eWeight").val() == "") {
        $(".eWeight").addClass("iserr");
        $("#snackbarerror").text("Weight is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    var postData = {
        "weight: ":$(".eWeight").val(),
        "clarity":$(".eClarity option:selected").text(), 
        "color": $(".eColor option:selected").text(),
        "primary_color": $(".ePColor option:selected").text(),
        "karat": + $(".likt .licurrent").text(),
        "quantity": $(".eQty").val()
    };
    $(".eAddr").val()? postData.description = $(".eAddr").val():'';
    $.ajax({
        url: makeeq_api + sessionStorage.prodid + "/?type=" + sessionStorage.type,
        type: 'post',
        headers: {
            "content-type": "application/json",
            "Authorization": "Token " + localStorage.wutkn
        },
        data: JSON.stringify(postData),
        success: function(data) {
            $("#snackbarsuccs").text("Enquiry has been made");
            showsuccesstoast();

        },
        error: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function wishs() {
    $.ajax({
        url: wishlist_api + sessionStorage.prodid + "/" + "?type=" + sessionStorage.type,
        type: 'post',
        headers: {
            "content-type": "application/json",
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            if (data.in_wish_list == true) {
                $(".wishbtn").text("IN WISHLIST");
                $("#snackbarerror").text("Added to wishlist");
                showerrtoast();
                localStorage.wish_count = Number(localStorage.wish_count) + 1;
                $(".top-mini-cart .number-items").text(Number($(".top-mini-cart .number-items").text()) + 1);
            } else {
                $(".wishbtn").text("ADD TO WISHLIST");
                $("#snackbarsuccs").text("Removed from wishlist");
                showsuccesstoast();
                localStorage.wish_count = Number(localStorage.wish_count) - 1;
                $(".top-mini-cart .number-items").text(Number($(".top-mini-cart .number-items").text()) - 1);
            }
            $(".wishdrop").siblings().remove();
            wishdrop(1);
            wishdrop(2);
        },
        error: function(data) {
            $(".delldr").hide();
            $(".delcls").click();
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

//function for star append
function starz(star, id, cl) {
    $("." + cl + id).empty();
    if (star) {
        var whole = Math.floor(star);
        var high = 5 - Math.ceil(star);
        var dec = (star * 10) % 10;
        for (var i = 0; i < whole; i++) {
            $("." + cl + id).append(`<li><a class="fa fa-star"></a></li>`);
        }
        if (dec > 4) {
            $("." + cl + id).append(`<li><a class="fa fa-star-half-o"></a></li>`);
        } else if (dec < 5 && dec != 0) {
            $("." + cl + id).append(`<li><a class="fa fa-star-o"></a></li>`);
        }
        for (var i = 0; i < high; i++) {
            $("." + cl + id).append(`<li><a class="fa fa-star-o"></a></li>`);
        }
    } else {
        for (var i = 0; i < 5; i++) {
            $("." + cl + id).append(`<li><a class="fa fa-star-o"></a></li>`);
        }
    }
}

function wish(el, id) {
    $.ajax({
        url: wishlist_api + id + "/" + "?type=" + sessionStorage.type,
        type: 'post',
        headers: {
            "content-type": "application/json",
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            if (data.in_wish_list == true) {
                $(el).children().addClass('wishtrue');
                $(el).parent().addClass('sparkle');
                $("#snackbarsuccs").text("Added to wishlist");
                showsuccesstoast();
                localStorage.wish_count = Number(localStorage.wish_count) + 1;
                $(".top-mini-cart .number-items").text(Number($(".top-mini-cart .number-items").text()) + 1);
            } else {
                $(el).children().removeClass('wishtrue');
                $(el).parent().removeClass('sparkle');
                $("#snackbarsuccs").text("Removed from wishlist");
                showsuccesstoast();
                localStorage.wish_count = Number(localStorage.wish_count) - 1;
                $(".top-mini-cart .number-items").text(Number($(".top-mini-cart .number-items").text()) - 1);
            }
            $(".wishdrop").siblings().remove();
            wishdrop(1);
            wishdrop(2);
        },
        error: function(data) {

            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function addreview() {
    if ($(".rating_val input:checked").length == 0) {
        // $(".rating_val input:").addClass("iserr");
        $("#snackbarerror").text("Rating is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($(".reviewcomm").val() == "") {
        $(".reviewcomm").addClass("iserr");
        $("#snackbarerror").text("Review description is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    var postData = JSON.stringify({
        rating: $(".rating_val input:checked").val() / 2,
        comment: $(".reviewcomm").val(),
        type: sessionStorage.type
    });
    $.ajax({
        url: addreview_api + sessionStorage.prodid + "/",
        type: "post",
        data: postData,
        headers: {
            "content-type": "application/json",
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".listreviews").append(`
                    <li>
                        <div class="user-comments">
                            <h4 class="heading-regular font15 font-montserrat text-normal" style="width: 260px;"> ${JSON.parse(localStorage.userdetails).first_name} <span style="float: right;"> ${data.rating} <a class="fa fa-star"></a></span></h4>
                            <h6 class="font-montserrat-light font13">${data.comment}</h6>
                        </div>
                    </li>
                `);
            $(".clsbtn").click();
            $("#snackbarsuccs").text("Review added succesfully");
            showsuccesstoast();

        },
        error: function(data) {
            $(".clsbtn").click();
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}


function proddetail(id) {
    sessionStorage.prodid = id;
    sessionStorage.type = 1;
    window.location.href = "product-detail.html";
}


$(".mega-menu").mouseover(() => {
    $(".zoomContainer").css("position", "");
});

$(".mega-menu").mouseleave(() => {
    $(".zoomContainer").css("position", "absolute");
});