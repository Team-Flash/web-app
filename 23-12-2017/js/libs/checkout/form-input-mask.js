var FormInputMask = function () {
    
    var handleInputMasks = function () {


        $("#input-ccv").inputmask({
            "mask": "9",
            "repeat": 3,
            "greedy": false
        }); // ~ mask "9" or mask "99" or ... mask "9999999999"

        $("#input-card-number").inputmask("9999-9999-9999-9999", {
            placeholder: " ",
            clearMaskOnLostFocus: true
        }); //default
    }

    var handleIPAddressInput = function () {
        $('#input_ipv4').ipAddress();
        $('#input_ipv6').ipAddress({
            v: 6
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleInputMasks();
            handleIPAddressInput();
        }
    };

}();

if (App.isAngularJsApp() === false) { 
    (function ($) {
        FormInputMask.init(); // init metronic core componets
    });
}